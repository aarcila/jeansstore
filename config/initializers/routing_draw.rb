# frozen_string_literal: true

# adds draw method into Rails routing
class ActionDispatch::Routing::Mapper
  # It allows us to keep routing splitted into files
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end
end
