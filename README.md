# README

* Ruby version - 3.0.0 - Rails Version 6.1.3

* CSS Framework UiKit https://getuikit.com

* Database Mysql2

* Design by MannyKitten https://www.behance.net/MannyKitten | https://www.behance.net/gallery/121457535/The-Jeans-Store

* Using simple form https://github.com/heartcombo/simple_form

* Using stock images from https://www.pexels.com/es-es/